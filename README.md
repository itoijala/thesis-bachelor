# Bachelor Thesis "Form Factors in B → K\* Decays"

Compiled PDFs in directory `final`.

## Required programs (tested versions):
* GNU make 3.82
* TeXLive 2013
* Python 3.3.2

## Python libraries (Python 3):
* numpy 1.7.1
* scipy 0.12.0
* pandas 0.11.0
* uncertainties 2.3.6
* matplotlib 1.2.1
* pyminuit2 1.1.0 (patched, see https://code.google.com/p/pyminuit/issues/detail?id=34#c1)
