\chapter{Fit}
\label{fit}
Here, data from lattice QCD simulations will be fitted in order to extrapolate the form factors to the whole physically relevant region of $t$.
The simplified series expansion
\begin{eqn}
  \f{T_i}(t) = \frac{1}{1 - \frac{t}{m_i^2}} \sum_{j = 0}^N α_{i, j} \f{z}(t)^j \eqlabel{eqn:series-expansion}
\end{eqn}
of order $N$ from \cite{1004.3249v4} is used to parameterise the form factors.
The mass
\begin{eqn}
  m_i = m_{\f{\PBs}(J^P)}
\end{eqn}
is the mass of the \PBs resonance of the appropriate spin and parity \cite{1004.3249v4}: $1^-$ for $T_1$ and $1^+$ for $T_2$.
The function
\begin{eqn}
  \f{z}(t) = \frac{\sqrt{t_+ - t} - \sqrt{t_+ - t_0}}{\sqrt{t_+ - t} + \sqrt{t_+ - t_0}}
\end{eqn}
maps $t$ to $\intoo{-1}{1}$ so that the power series in $\f{z}(t)$ converges.
The parameters used in $\f{z}(t)$ are
\begin{eqn}
  t_± = \g(m_{\PB} ± m_{\PKstar})^2
\end{eqn}
and
\begin{eqn}
  t_0 = t_+ \g(1 - \sqrt{1 - \frac{t_-}{t_+}}) ,
\end{eqn}
where $t_0$ is chosen to be optimal in the sense that the maximum of $\abs{\f{z}(t)}$ is minimal.
Using \eqref{eqn:T_1-T_2}, one of the parameters $α_{i, j}$ can be written as a linear combination of the others.
Here,
\begin{eqn}
  α_{2, 0} = α_{1, 0} + \sum_{j = 1}^N \g(α_{1, j} - α_{2, j}) \f{z}(0)^j
\end{eqn}
is used.

The $χ^2$ fit is performed by minimising the sum
\begin{eqn}
  χ^2 = \sum_{i = 1}^2 \sum_{k = 0}^{n_i} \g(\frac{\f{T_i}(t_{i, k}) - T_{i, k}}{\err{T_{i, k}}})^{\!\! 2}
\end{eqn}
of the residuals where $\g(t_{i, k}, T_{i, k} ± \err{T_{i, k}})$ are the $n_i$ lattice data points for the form factor $T_i$.
This leaves
\begin{eqn}
  f = n_1  + n_2 - \g(2 N + 1)
\end{eqn}
as the number of degrees of freedom.
The MIGRAD algorithm from the MINUIT2 package in ROOT \cite{root} is used for the minimisation.
The accompanying HESSE algorithm is used to calculate the covariance matrix of the fit parameters.

The values of the constants used in the fit are shown in table \ref{tab:constants}.
The lattice QCD results from \cite{mescia} (set 3) and \cite{wingateB2V} (set f0062) are shown in tables \ref{tab:data-T1-T2-mescia} and \ref{tab:data-T1-T2-wingate}, respectively.
The results in \cite{mescia} were calculated using the older quenched approximation while the newer results in \cite{wingateB2V} stem from unquenched $2 + 1 $ flavour calculations \cite{wingate}.
The form factors can also be calculated for small $t$ using light-cone sum rules (LCSR).
An LCSR result for $t = 0$, which is used for comparison and is not included in the fit, is \cite{1006.4945v2}
\begin{eqn}
  \f{T_1}(0) = \f{T_2}(0) = \input{result/lcsr.tex} ^+_-{} \input{result/lcsr-error.tex} .
\end{eqn}
\begin{table}
  \caption{Constants used in the fit: \PB and \PKstar meson masses $m_{\PB, \PKstar}$ from \cite{pdg2012} and resonance masses $m_i$ from \cite{1004.3249v4}.}
  \label{tab:constants}
  \input{table/constants.tex}
\end{table}
\begin{table}
  \caption{Lattice QCD data from \cite{mescia} for the form factors $T_1$ and $T_2$.}
  \label{tab:data-T1-T2-mescia}
  \input{table/data-T1-T2-mescia.tex}
\end{table}
\begin{table}
  \caption{Lattice QCD data from \cite{wingateB2V} for the form factors $T_1$ and $T_2$.}
  \label{tab:data-T1-T2-wingate}
  \input{table/data-T1-T2-wingate.tex}
\end{table}
\section{Order \texorpdfstring{$N = 0$}{N = 0}}
\label{N=0}
The results of the fit with order $N = 0$ using the lattice data from \cite{wingateB2V} are shown in table \ref{tab:fit-0-wingate} and plotted in figure \ref{fig:T-0-wingate}.
As seen in the figure, there are not enough fit parameters to describe both form factors.
This is due to the fact that with only one fit parameter, the only difference between the form factors is the resonance mass used, making the form factors linearly dependent and meaning that a fit with at least $N = 1$ is necessary.
\begin{figure}
  \includegraphics[width=\textwidth]{graphic/T-0-wingate.pdf}
  \caption{
    Lattice QCD data from \cite{wingateB2V} and fit results for $N = 0$.
    Shaded regions are the $1σ$ and $2σ$ areas.
  }
  \label{fig:T-0-wingate}
\end{figure}
\begin{table}
  \caption{Results of the fit with order $N = 0$ using the data from \cite{wingateB2V}.}
  \label{tab:fit-0-wingate}
  \input{table/fit-0-wingate.tex}
\end{table}
\section{Order \texorpdfstring{$N = 1$}{N = 1}}
\subsection{Quenched Lattice QCD}
\label{quenched}
The results of the fit with order $N = 1$ using the lattice data from \cite{mescia} are shown in table \ref{tab:fit-1-mescia} and plotted in figure \ref{fig:T-1-mescia}.
The correlation matrix of the fit parameters is shown in figure \ref{fig:matrix-1-mescia}.
The fact that $χ^2 / f$ is smaller than one can be explained by the fact that correlations between the errors of the data points are not taken into account as the correlations have not been published.
However, because the same simulation is used to produce all the data, the correlations should be large.

Figure \ref{fig:contour-1-mescia} shows contours of $χ^2$ as a function of a pair of parameters.
The other parameters are held at their values from the function\textquoteright{}s minimum.
The parameter $α_{2, 0}$ is not included in the contour plots, because it is calculated from the other parameters and is not part of the fit.
The contours are calculated using MINUIT2\textquoteright{}s CONTOUR algorithm, which finds a set of points where the function takes on a value of
\begin{eqn}
  χ^2 = χ^2_{\t{min}} + \f{F^{-1}}(c, 2) ,
\end{eqn}
where $\f{F^{-1}}(c, 2)$ is the inverse cumulative density function of the $χ^2$-distribution for two degrees of freedom and $c$ the desired confidence level.
The plots show both \SI{68}{\percent} and \SI{95}{\percent} confidence regions.
All of the contours are ellipses centred at the minimum, meaning that the function is parabolic at its minimum, allowing the use of symmetric uncertainties and making it unnecessary to use the MINOS routine from MINUIT2, since it is used to calculate asymmetric errors for non-parabolic functions.
\begin{figure}
  \includegraphics[width=\textwidth]{graphic/T-1-mescia.pdf}
  \caption{
    Lattice QCD data from \cite{mescia} and fit results for $N = 1$.
    Shaded regions are the $1σ$ and $2σ$ areas.
  }
  \label{fig:T-1-mescia}
\end{figure}
\begin{table}
  \caption{Results of the fit with order $N = 1$ using the data from \cite{mescia}.}
  \label{tab:fit-1-mescia}
  \input{table/fit-1-mescia.tex}
\end{table}
\begin{figure}
  \includegraphics[width=\textwidth]{graphic/contour-1-mescia.pdf}
  \caption{
    Contour plots of the fit with order $N = 1$ using the data from \cite{mescia}.
    The parameters not plotted are held constant at their values in the minimum for each pair of parameters.
    Both \SI{68}{\percent} and \SI{95}{\percent} confidence regions are shown.
  }
  \label{fig:contour-1-mescia}
\end{figure}
\begin{figure}
  \includegraphics{graphic/matrix-1-mescia.pdf}
  \caption{Correlation matrix of the parameters of the fit with order $N = 1$ using the data from \cite{mescia}.}
  \label{fig:matrix-1-mescia}
\end{figure}
\FloatBarrier
\subsection{Unquenched, \texorpdfstring{$2 + 1$}{2 + 1} Flavour Lattice QCD}
The results of the fit with order $N = 1$, using the lattice data from \cite{wingateB2V}, are shown in table \ref{tab:fit-1-wingate} and plotted in figure \ref{fig:T-1-wingate}.
The contours are shown in figure \ref{fig:contour-1-wingate}.
The correlations between the fit parameters are shown in figure \ref{fig:matrix-1-wingate}.
The relative error of $α_{2, 1}$ is very large, although this does not lead to large uncertainties in the form factor.
The statements about $χ^2 / f$ in section \ref{quenched} also apply in this section.
\begin{figure}
  \includegraphics[width=\textwidth]{graphic/T-1-wingate.pdf}
  \caption{
    Lattice QCD data from \cite{wingateB2V} and fit results for $N = 1$.
    Shaded regions are the $1σ$ and $2σ$ areas.
  }
  \label{fig:T-1-wingate}
\end{figure}
\begin{table}
  \caption{Results of the fit with order $N = 1$ using the data from \cite{wingateB2V}.}
  \label{tab:fit-1-wingate}
  \input{table/fit-1-wingate.tex}
\end{table}
\begin{figure}
  \includegraphics[width=\textwidth]{graphic/contour-1-wingate.pdf}
  \caption{
    Contour plots of the fit with order $N = 1$ using the data from \cite{wingateB2V}.
    The parameters not plotted are held constant at their values in the minimum for each pair of parameters.
    Both \SI{68}{\percent} and \SI{95}{\percent} confidence regions are shown.
  }
  \label{fig:contour-1-wingate}
\end{figure}
\begin{figure}
  \includegraphics{graphic/matrix-1-wingate.pdf}
  \caption{Correlation matrix of the parameters of the fit with order $N = 1$ using the data from \cite{wingateB2V}.}
  \label{fig:matrix-1-wingate}
\end{figure}
\FloatBarrier
\section{Order \texorpdfstring{$N = 2$}{N = 2}}
\label{N=2}
The results of the fit with order $N = 2$ using the lattice data from \cite{wingateB2V} are shown in table \ref{tab:fit-2-wingate} and plotted in figure \ref{fig:T-2-wingate}.
There is not enough data to be able to perform a fit with $N = 2$ reasonably.
This is evidenced by the large errors of the parameters in table \ref{tab:fit-2-wingate}.
The correlations between the parameters, as shown in figure \ref{fig:matrix-2-wingate}, are also very large.
The contours are shown in figure \ref{fig:contour-2-wingate}.
The fact that $χ^2 / f$ is significantly smaller than one means that the model with $N = 2$ is overfitting the data, meaning that a model with less fit parameters is preferable.
\begin{figure}
  \includegraphics[width=\textwidth]{graphic/T-2-wingate.pdf}
  \caption{Lattice QCD data from \cite{wingateB2V} and fit results for $N = 2$.
           Shaded regions are the $1σ$ and $2σ$ areas.}
  \label{fig:T-2-wingate}
\end{figure}
\begin{table}
  \caption{Results of the fit with order $N = 2$ using the data from \cite{wingateB2V}.}
  \label{tab:fit-2-wingate}
  \input{table/fit-2-wingate.tex}
\end{table}
\begin{figure}
  \includegraphics{graphic/matrix-2-wingate.pdf}
  \caption{Correlation matrix of the parameters of the fit with order $N = 2$ using the data from \cite{wingateB2V}.}
  \label{fig:matrix-2-wingate}
\end{figure}
\begin{figure}
  \includegraphics[width=\textwidth]{graphic/contour-2-wingate.pdf}
  \caption{
    Contour plots of the fit with order $N = 2$ using the data from \cite{wingateB2V}.
    The parameters not plotted are held constant at their values in the minimum for each pair of parameters.
    Both \SI{68}{\percent} and \SI{95}{\percent} confidence regions are shown.
  }
  \label{fig:contour-2-wingate}
\end{figure}
\section{Comparison of Quenched and Unquenched Fit Results}
Sections \ref{N=0} and \ref{N=2} explain why fits with orders $N = 0$ and $N = 2$, respectively, fail.
Consequently, this section concentrates on the results with order $N = 1$.
More data is available for the quenched approximation than for the unquenched calculation.
This results in smaller correlations for the quenched than for the unquenched results.
The deviation from the LCSR value for the quenched data, though larger than for the unquenched data, is not statistically significant.
The agreement of the unquenched fit result and the LCSR value is good.
The LCSR value and the predictions of the fit results for $t = 0$ are shown in table \ref{tab:T(0)}.
Both form factors take on a smaller value at $t = t_-$ for the unquenched than for the quenched data, meaning that the different lattice QCD methods produce qualitatively different results.
\begin{table}
  \caption{LCSR value from \cite{1006.4945v2} and the predictions of the fit results for $\f{T_1}(0) = \f{T_2}(0)$.}
  \label{tab:T(0)}
  \input{table/t=0.tex}
\end{table}
\section{Consistency Check Using Isgur\ndash{}Wise Relations}
The Isgur\ndash{}Wise relations \eqref{eqn:isgur-wise1} and \eqref{eqn:isgur-wise2} can be used to check the consistency of a calculation that produces both dipole and semileptonic form factors.
It follows that
\begin{eqn}
  \frac{T_1}{T_2} = \frac{V}{A_1} + \LandauO(\frac{1}{m_{\Pbottom}}) .
\end{eqn}
Table \ref{tab:data-V-A1-wingate} includes the lattice data for $V$ and $A_1$ from \cite{wingateB2V}.
The fit parameters for $N = 1$ from table \ref{tab:fit-1-wingate} are used to plot $T_1 / T_2$ in figure \ref{fig:quotient}.
The points are the values for $V / A_1$, plotted at the mean of their respective $t$ values.
The deviation is not statistically significant as the values agree to within two standard deviations.
Furthermore, due to the corrections of order $1 / m_{\Pbottom}$, the agreement cannot be expected to be exact.
\begin{table}
  \caption{Lattice QCD data from \cite{wingateB2V} for the form factors $V$ and $A_1$.}
  \label{tab:data-V-A1-wingate}
  \input{table/data-V-A1-wingate.tex}
\end{table}
\begin{figure}
  \includegraphics[width=\textwidth]{graphic/quotient.pdf}
  \caption{
    Curve for $T_1 / T_2$, using fit results from table \ref{tab:fit-1-wingate} shown with both $1σ$ and $2σ$ regions shaded dark and light, respectively.
    Lattice data for $V / A_1$ taken from table \ref{tab:data-V-A1-wingate}.
  }
  \label{fig:quotient}
\end{figure}
