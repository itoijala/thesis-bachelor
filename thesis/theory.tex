\chapter{Theory}
\label{theory}
\section{Kinematics}
\label{kinematics}
The kinematics of the decay \HepProcess{\PB \to \PKstar \, \Pleptonplus \, \Pleptonminus} is characterised by the momenta $\ten{p}_{\PB}$ and $\ten{p}_{\PKstar}$ of the \PB and \PKstar mesons, respectively.
The momentum transfer is defined as $\ten{q} \coloneq \ten{p}_{\PB} - \ten{p}_{\PKstar}$ with $t \coloneq \ten{q}^2$.
The physical limits for $t$ follow from four-momentum conservation in the rest frame of the \PB meson.
In the limit that the \PKstar meson is also at rest in this frame, it follows that $t = \g(m_{\PB} - m_{\PKstar})^2$, where $m_{\PB}$ and $m_{\PKstar}$ are the masses of the \PB and the \PKstar mesons, respectively.
In the other limit, where the \PKstar meson has maximal energy in the \PB meson\textquoteright{}s rest frame, it follows that $t = 4 m_{\Plepton}^2$ where $m_{\Plepton}$ is the mass of the created lepton.
In summary, the physically accessible region is
\begin{eqn}
  4 m_{\Plepton}^2 \leq t \leq \g(m_{\PB} - m_{\PKstar})^2 .
\end{eqn}
\section{Form Factor Definitions}
\label{definition}
The amplitude of the decay \HepProcess{\PB \to \PKstar \, \Pleptonplus \, \Pleptonminus} is proportional to the matrix element
\begin{eqn}
  \braket{\rule{0pt}{1em} \f{\PKstar}(\ten{p}_{\PKstar}, \ten{η})}[\bar{s} Γ b]{\f{\PB}(\ten{p}_{\PB})} ,
\end{eqn}
where $b$ is the incoming bottom quark field, $\bar{s}$ the outgoing strange quark field and $Γ$ a Dirac matrix.
The combination $\bar{s} Γ b$ is called a current.
The polarisation $\ten{η}$ of the \PKstar meson, which is of spin $1$ while the \PB meson is of spin $0$, is orthogonal to its momentum, meaning $\tn{η^*}{_μ} \tn{p_{\PKstar}}{^μ} = 0$.

The matrix element can be decomposed into parts following the rules that parity must be preserved and every part must be proportional to $\ten{η}^*$.
There are two Dirac matrices giving non-zero contributions to the amplitude: $Γ = \tn{γ}{_μ} \g(1 - γ_5)$, which leads to the semileptonic form factors, and $Γ = \tn{σ}{_μ_ν} \tn{q}{^ν} \g(1 + γ_5)$, which leads to the dipole form factors.
The matrices $γ_5$ and $\tn{σ}{^μ^ν}$ are defined by
\begin{eqn}
  γ_5 = \I \tn{γ}{^0} \tn{γ}{^1} \tn{γ}{^2} \tn{γ}{^3} \\
  \tn{σ}{^μ^ν} = \frac{\I}{2} \g(\tn{γ}{^μ} \tn{γ}{^ν} - \tn{γ}{^ν} \tn{γ}{^μ}) ,
\end{eqn}
where the $\tn{γ}{^μ}$ are the Dirac matrices.

The dipole current can be divided into two terms with even and odd parity, respectively:
\begin{eqn}
  \braket{\PKstar}[\bar{s} \tn{σ}{_μ_ν} \tn{q}{^ν} \g(1 + γ_5) b]{\PB}
    = \braket{\PKstar}[\bar{s} \tn{σ}{_μ_ν} \tn{q}{^ν} b]{\PB} + \braket{\PKstar}[\bar{s} \tn{σ}{_μ_ν} \tn{q}{^ν} γ_5 b]{\PB} .
\end{eqn}
The only term with even parity that can be constructed from the vectors $\ten{η}^*$, $\ten{p}_{\PB}$ and $\ten{p}_{\PKstar}$ is
\begin{eqns}
  \braket{\PKstar}[\bar{s} \tn{σ}{_μ_ν} \tn{q}{^ν} b]{\PB}
    &=& \LeviCivita{_μ_ν_ρ_σ} \tn{η^*}{^ν} \tn{\g(c_1 p_{\PB} + c_2 p_{\PKstar})}{^ρ} \tn{\g(c_3 p_{\PB} + c_4 p_{\PKstar})}{^σ} \IEEEyesnumber \IEEEyessubnumber* \\
  &=& \LeviCivita{_μ_ν_ρ_σ} \tn{η^*}{^ν}
    \g{(}{\vphantom{
      c_1 c_3 \tn{p_{\PB}}{^ρ} \tn{p_{\PB}}{^σ}
      + c_2 c_4 \tn{p_{\PKstar}}{^ρ} \tn{p_{\PKstar}}{^σ}
      + \g(c_1 c_4 - c_2 c_3) \tn{p_{\PB}}{^ρ} \tn{p_{\PKstar}}{^σ}
    }}{.}\!
    \underbrace{
      c_1 c_3 \tn{p_{\PB}}{^ρ} \tn{p_{\PB}}{^σ}
    }_{{} = 0}
    +
    \underbrace{
      c_2 c_4 \tn{p_{\PKstar}}{^ρ} \tn{p_{\PKstar}}{^σ}
    }_{{} = 0}
    + \g(c_1 c_4 - c_2 c_3) \tn{p_{\PB}}{^ρ} \tn{p_{\PKstar}}{^σ}
    \!\g{.}{\vphantom{
      c_1 c_3 \tn{p_{\PB}}{^ρ} \tn{p_{\PB}}{^σ}
      + c_2 c_4 \tn{p_{\PKstar}}{^ρ} \tn{p_{\PKstar}}{^σ}
      + \g(c_1 c_4 - c_2 c_3) \tn{p_{\PB}}{^ρ} \tn{p_{\PKstar}}{^σ}
    }}{)} \IEEEeqnarraynumspace \\
  &=& 2 \I \f{T_1}(t) \LeviCivita{_μ_ν_ρ_σ} \tn{η^*}{^ν} \tn{p_{\PB}}{^ρ} \tn{p_{\PKstar}}{^σ} ,
\end{eqns}
where the $c_i$ are functions of $t$ and the terms in the third step are zero due to the antisymmetry of the Levi-Civita symbol $\LeviCivita{}$.
The form factor $T_1$ can depend only on $t$, since it is the only available Lorentz invariant.

As a result of
\begin{eqn}
  \tn{p_{\PB}}{_μ} \tn{p_{\PKstar}}{^μ} = \frac{1}{2} \g(m_{\PB}^2 + m_{\PKstar}^2 - t) , \label{eqn:pB-pKstar}
\end{eqn}
there are only three linearly independent terms with odd parity that can be constructed from the available vectors:
\begin{eqns}
  \braket{\PKstar}[\bar{s} \tn{σ}{_μ_ν} \tn{q}{^ν} γ_5 b]{\PB}
    &=& \f{T_2}(t) \g(m_{\PB}^2 - m_{\PKstar}^2) \tn{η^*}{_μ}
    + \f{T_3}(t) \tn{η^*}{_ν} \tn{p_{\PB}}{^ν} \tn{q}{_μ} \IEEEnonumber \\
    & & {} + a \tn{η^*}{_ν} \tn{p_{\PB}}{^ν} \tn{\g(p_{\PB} + p_{\PKstar})}{_μ} ,
\end{eqns}
where $T_2$ and $T_3$ are form factors and $a$ is a function of $t$.
The function $a$ can be calculated using the antisymmetry of the $\ten{σ}$ tensor, from which follows that
\begin{eqn}
  \braket{\PKstar}[\bar{s} \tn{σ}{_μ_ν} \tn{q}{^ν} \g(1 + γ_5) b]{\PB} \tn{q}{^μ} = 0 .
\end{eqn}
The term of even parity already satisfies this, leading to the demand that
\begin{eqns}
  0 &=& \braket{\PKstar}[\bar{s} \tn{σ}{_μ_ν} \tn{q}{^ν} γ_5 b]{\PB} \tn{q}{^μ} \IEEEyesnumber \IEEEyessubnumber* \\
  &=& \f{T_2}(t) \g(m_{\PB}^2 - m_{\PKstar}^2) \tn{η^*}{_μ} \tn{q}{^μ}
    + \f{T_3}(t) \tn{η^*}{_ν} \tn{p_{\PB}}{^ν} \tn{q}{_μ} \tn{q}{^μ}
    + a \tn{η^*}{_ν} \tn{p_{\PB}}{^ν} \tn{\g(p_{\PB} + p_{\PKstar})}{_μ} \tn{q}{^μ} \IEEEeqnarraynumspace \\
  &=& \f{T_2}(t) \g(m_{\PB}^2 - m_{\PKstar}^2) \tn{η^*}{_μ} \tn{p_{\PB}}{^μ}
    + \f{T_3}(t) \tn{η^*}{_ν} \tn{p_{\PB}}{^ν} t
    + a \tn{η^*}{_ν} \tn{p_{\PB}}{^ν} \g(m_{\PB}^2 - m_{\PKstar}^2) ,
\end{eqns}
which is satisfied by
\begin{eqn}
  a = - \f{T_2}(t) - \frac{t}{m_{\PB}^2 - m_{\PKstar}^2} \f{T_3}(t) .
\end{eqn}
This completes the decomposition of the matrix element, giving
\begin{eqns}
  \braket{\PKstar}[\bar{s} \tn{σ}{_μ_ν} \tn{q}{^ν} \g(1 + γ_5) b]{\PB}
    &=& 2 \I \f{T_1}(t) \LeviCivita{_μ_ν_ρ_σ} \tn{η^*}{^ν} \tn{p_{\PB}}{^ρ} \tn{p_{\PKstar}}{^σ} \IEEEnonumber* \\
    & & {} + \f{T_2}(t) \g(\g(m_{\PB}^2 - m_{\PKstar}^2) \tn{η^*}{_μ} - \tn{η^*}{_ν} \tn{p_{\PB}}{^ν} \tn{\g(p_{\PB} + p_{\PKstar})}{_μ}) \\
    & & {} + \f{T_3}(t) \tn{η^*}{_ν} \tn{p_{\PB}}{^ν} \g(\tn{q}{_μ} - \frac{t}{m_{\PB}^2 - m_{\PKstar}^2} \tn{\g(p_{\PB} + p_{\PKstar})}{_μ}) . \IEEEyesnumber \IEEEeqnarraynumspace
\end{eqns}

The decomposition of the semileptonic matrix element \cite{9910221v2}
\begin{eqns}
  \braket{\PKstar}[\bar{s} \tn{γ}{_μ} \g(1 - γ_5) b]{\PB}
    &=& - \I \g(m_{\PB} + m_{\PKstar}) \f{A_1}(t) \tn{η^*}{_μ}
    + \frac{\I \f{A_2}(t)}{m_{\PB} + m_{\PKstar}} \tn{η^*}{_ν} \tn{p_{\PB}}{^ν} \tn{\g(p_{\PB} + p_{\PKstar})}{_μ} \IEEEnonumber* \\
    & & {} + \frac{2 \I m_{\PKstar}}{t} \g(\f{A_3}(t) - \f{A_0}(t)) \tn{η^*}{_ν} \tn{p_{\PB}}{^ν} \tn{q}{_μ} \\
    & & {} + \frac{2 \f{V}(t)}{m_{\PB} + m_{\PKstar}} \LeviCivita{_μ_ν_ρ_σ} \tn{η^*}{^ν} \tn{p_{\PB}}{^ρ} \tn{p_{\PKstar}}{^σ} \IEEEyesnumber
\end{eqns}
defines the semileptonic form factors $A_0$, $A_1$, $A_2$, $A_3$ and $V$.
\section{Form Factor Relations}
\subsection{\texorpdfstring{$T_1$}{T₁} and \texorpdfstring{$T_2$}{T₂} in the large recoil limit}
The relation
\begin{eqn}
  \f{T_1}(0) = \f{T_2}(0) \eqlabel{eqn:T_1-T_2}
\end{eqn}
is essential to the fit method used in chapter \ref{fit} and can be shown using
\begin{eqn}
  \tn{σ}{_μ_ν} γ_5 = - \frac{\I}{2} \LeviCivita{_μ_ν_α_β} \tn{σ}{^α^β} .
\end{eqn}
This gives, using the shorthand $\braket{\PKstar}[\bar{s} \ldots b]{\PB} = \avg{\ldots}$,
\begin{eqn}
  2 \I T_1 \LeviCivita{_μ_ν_ρ_σ} \tn{η^*}{^ν} \tn{p_{\PB}}{^ρ} \tn{p_{\PKstar}}{^σ} = \avg{\tn{σ}{_μ_ν} \tn{q}{^ν}} = - \frac{\I}{2} \LeviCivita{_μ_ν_ρ_σ} \avg{\tn{σ}{^ρ^σ} \tn{q}{^ν} γ_5} .
\end{eqn}
Contracting both sides with $\LeviCivita{^μ^α^β^γ}$ and using the relation \cite{9507456v1}
\begin{eqn}
  \LeviCivita{_μ_ν_ρ_σ} \LeviCivita{^μ^α^β^γ} = - \begin{vmatrix}
    \Kroneckerdelta{}{^α_ν} & \Kroneckerdelta{}{^α_ρ} & \Kroneckerdelta{}{^α_σ} \\
    \Kroneckerdelta{}{^β_ν} & \Kroneckerdelta{}{^β_ρ} & \Kroneckerdelta{}{^β_σ} \\
    \Kroneckerdelta{}{^γ_ν} & \Kroneckerdelta{}{^γ_ρ} & \Kroneckerdelta{}{^γ_σ}
  \end{vmatrix} ,
\end{eqn}
then contracting with $\tn{q}{_γ}$ and using \eqref{eqn:pB-pKstar} and finally specialising to the case where $t = 0$ gives
\begin{eqns}[l]
  \f{T_1}(0) \g(\tn{q}{^α} \g(\g(m_{\PB}^2 - m_{\PKstar}^2) \tn{η^*}{^β} - 2 \tn{η^*}{_μ} \tn{p_{\PB}}{^μ} \tn{p_{\PB}}{^β})
  - \tn{q}{^β} \g(\g(m_{\PB}^2 - m_{\PKstar}^2) \tn{η^*}{^α} - 2 \tn{η^*}{_μ} \tn{p_{\PB}}{^μ} \tn{p_{\PB}}{^α})) \IEEEnonumber \\
  \quad\qquad = \tn{q}{^α} \avg{\tn{σ}{^β^μ} \tn{q}{_μ} γ_5} - \tn{q}{^β} \avg{\tn{σ}{^α^μ} \tn{q}{_μ} γ_5} .
\end{eqns}
Inserting for the matrix elements the terms with $T_2$ and $T_3$ and contracting with $\tn{q}{_β}$ gives, for $t = 0$,
\begin{eqn}
  \g(\f{T_1}(0) - \f{T_2}(0)) \tn{η^*}{_μ} \tn{p_{\PB}}{^μ} \tn{q}{^α} = 0 ,
\end{eqn}
from which the relation \eqref{eqn:T_1-T_2} follows.
\subsection{Isgur\ndash{}Wise Relations}
The improved Isgur\ndash{}Wise relations \cite{1006.5013v2}
\begin{eqns}[rClCl]
  \f{T_1}(t) &=& κ \f{V}(t)   &+& \LandauO(\frac{1}{m_{\Pbottom}}) \eqlabel{eqn:isgur-wise1}
  \itext{and}
  \f{T_2}(t) &=& κ \f{A_1}(t) &+& \LandauO(\frac{1}{m_{\Pbottom}}) \eqlabel{eqn:isgur-wise2}
\end{eqns}
with a constant $κ$ relate the dipole form factors $T_1$ and $T_2$ to the semileptonic form factors $V$ and $A_1$ for large $t$.
These relations follow from the heavy quark effective theory.
