\chapter{Introduction}
\label{introduction}
High energy physics (HEP) or particle physics is the study of elementary particles and their interactions.
Both are described with high accuracy by the standard model (SM) of particle physics in which matter is built from fermions, namely quarks and leptons, interacting through the exchange of gauge bosons.
The elementary particles of the standard model are shown in figure \ref{fig:standard-model}.
The photon \Pphoton is the mediator of the electromagnetic force and the gluon \Pgluon, of which there exist eight different versions with different colour charges, that of the strong force.
The weak force is mediated by the charged \PWpm bosons and the neutral \PZzero boson.
The Higgs boson \PHiggs is responsible for the masses of the particles in the standard model through the Higgs mechanism.
The standard model cannot be complete as it does not explain phenomena such as dark matter and dark energy or the amount of $CP$ violation necessary to account for the observed matter-antimatter asymmetry of the universe.
\begin{figure}
  \input{graphic/standard-model.tex}
  \caption{The particles described by the standard model of particle physics.
           Quarks are shown in green, leptons in blue, gauge bosons in red and the Higgs boson in grey.}
  \label{fig:standard-model}
\end{figure}

Flavour physics is the study of the different flavours of quarks and leptons.
Especially interesting are changes of flavour, which are forbidden by the electromagnetic and strong forces but allowed by the weak force through the exchange of \PW bosons.
There are two types of effective interactions that can change the flavour of quarks and leptons: charged currents (CC) and flavour changing neutral currents (FCNC).
Charged currents arise at tree level while flavour changing neutral currents can only occur at loop level and are therefore suppressed.

Studying rare decays, meaning decays with a small branching ratio in the standard model, is an important method for searching for new physics beyond the standard model.
It is hoped that experiments measuring rare decays will find deviations from standard model predictions, which could then be used to test new theories beyond the standard model.

One such process is \HepProcess{\PB \to \PKstar \, \Pleptonplus \, \Pleptonminus} in which a \PB meson, composed of a bottom antiquark and an up or a down quark (using the conventions in \cite{pdg2012}), decays into a \PKstar meson, composed of a strange antiquark and a lighter quark, and two matching leptons.
The coupling constant $α_{\t{s}}$ of quantum chromodynamics (QCD) increases for decreasing energy, rendering perturbative methods useless for quarks which are confined in hadrons.
The effects of QCD at low energy are encoded in the so-called form factors that are part of the expression for amplitude of the decay.
Figure \ref{fig:feynman} shows an illustrative Feynman diagram for this decay.
\begin{figure}
  \input{graphic/feynman.tex}
  \caption{An illustrative Feynman diagram for the process \HepProcess{\PB \to \PKstar \, \Pleptonplus \, \Pleptonminus}.}
  \label{fig:feynman}
\end{figure}

Kinematic regions can be classified by the invariant dilepton mass $\ten{q}^2 = \g(\ten{p}_{\Pleptonplus} + \ten{p}_{\Pleptonminus})^2$, $\ten{p}_{\Pleptonpm}$ being the momenta of the outgoing leptons.
Different kinematic regions are only suitable for the application of different theoretical methods.
In the low recoil region, in which the \PKstar meson softly recoils against the \PB meson, i.e. in the large $\ten{q}^2$ region, lattice QCD is applicable.
The large recoil region on the other hand is accessible by light-cone sum rules (LCSR).

In the so-called heavy quark effective theory, which is based on an expansion in the small parameter $Λ_{\t{QCD}} / m_{\Pbottom}$, relations between different form factors can be defined.
The quantities $Λ_{\t{QCD}}$ and $m_{\Pbottom}$ are the QCD scale and the mass of the bottom quark, respectively.

The form factors are defined in chapter \ref{theory}, including the necessary relations.
In chapter \ref{fit}, the form factors are extrapolated to all kinematic regions using a fit of current lattice data.
