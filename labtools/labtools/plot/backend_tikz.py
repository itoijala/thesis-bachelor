from matplotlib.figure import Figure
from matplotlib.path import Path
from matplotlib.backend_bases import RendererBase, GraphicsContextBase, FigureManagerBase, FigureCanvasBase
import pexpect

_latex = "bash -c 'TEXINPUTS=./build/:./:../:../powertools/: max_print_line=1048576 lualatex --shell-escape --output-directory=build'"
_header = "header/matplotlib.tex"

class RendererTikz(RendererBase):
    def __init__(self):
        self.dpi = 72.27
        self.latex_cache = dict()
        self.latex = pexpect.spawn(_latex)
        self.latex.delaybeforesend = 0
        self.latex.expect(r'\*\*')
        self.latex.sendline(r'\input{{{:s}}}'.format(_header))
        self.latex.expect(r'\*')
        self.latex.sendline(r'\begin{document}')
        self.latex.expect(r'\*')
        self.latex.sendline(r'\newbox\my')
        self.latex.expect(r'\*')

    def set_stream(self, stream):
        self.stream = stream

    def _parse_path(self, path, transform):
        parts = []
        for coords, t in path.iter_segments(transform=transform, remove_nans=True):
            if t == Path.MOVETO:
                parts.append("({:f},{:f})".format(coords[0], coords[1]))
            elif t == Path.LINETO:
                parts.append("-- ({:f}, {:f})".format(coords[0], coords[1]))
            elif t == Path.CURVE3:
                parts.append(".. controls ({:f},{:f}) .. ({:f},{:f})".format(coords[0], coords[1], coords[2], coords[3]))
            elif t == Path.CURVE4:
                parts.append(".. controls ({:f},{:f}) and ({:f},{:f}).. ({:f},{:f})".format(coords[0], coords[1], coords[2], coords[3], coords[4], coords[5]))
            elif t == Path.CLOSEPOLY:
                parts.append("-- cycle")
        return parts

    def draw_path(self, gc, path, transform, rgbFace=None):
        if gc.get_clip_path()[0] != None:
            clippath = self._parse_path(*gc.get_clip_path())
        elif gc.get_clip_rectangle() != None:
            rect = gc.get_clip_rectangle()
            clippath = ["({:f}, {:f})".format(rect.x0, rect.y0), "rectangle ({:f}, {:f})".format(rect.x1, rect.y1)]
        else:
            clippath = None
        drawpath = self._parse_path(path, transform)

        options = []
        options.append("line width={:f}pt".format(gc.get_linewidth()))
        linestyle = gc.get_linestyle("") # bug, needs argument, but unused
        if linestyle == "solid":
            options.append("solid")
        elif linestyle == "dashed":
            options.append("dashed")
        elif linestyle == "dotted":
            options.append("dotted")
        elif linestyle == "dashdot":
            options.append("dashdotted")
        joinstyle = gc.get_joinstyle()
        if joinstyle == "miter":
            options.append("line join=miter")
        elif joinstyle == "round":
            options.append("line join=round")
        elif joinstyle == "bevel":
            options.append("line join=bevel")
        capstyle = gc.get_capstyle()
        if capstyle == "butt":
            options.append("line cap=butt")
        elif capstyle == "round":
            options.append("line cap=round")
        elif capstyle == "projecting":
            options.append("line cap=rect")

        rgb = gc.get_rgb()
        if len(rgb) > 3:
            alpha = rgb[3]
        else:
            alpha = gc.get_alpha()
        options.append("draw opacity={:f}".format(alpha))
        edgecolor = r"\definecolor{{edgecolor}}{{rgb}}{{{:f},{:f},{:f}}}".format(rgb[0], rgb[1], rgb[2])
        options.append("draw=edgecolor")
        if rgbFace != None:
            if len(rgbFace) > 3:
                alphaFace = rgbFace[3]
            else:
                alphaFace = gc.get_alpha()
            options.append("fill opacity={:f}".format(alphaFace))
            fillcolor = r"\definecolor{{fillcolor}}{{rgb}}{{{:f},{:f},{:f}}}".format(rgbFace[0], rgbFace[1], rgbFace[2])
            options.append("fill=fillcolor")
        else:
            fillcolor = None

        result = r"    \begin{scope}" + "\n"
        result += "      {:s}\n".format(edgecolor)
        if fillcolor != None:
            result += "      {:s}\n".format(fillcolor)
        if clippath != None:
            result += (r"      \clip {:s};" + "\n").format(" ".join(clippath))
        result += (r"      \draw [{:s}] {:s};" + "\n").format(",".join(options), " ".join(drawpath))
        result += r"    \end{scope}" + "\n"
        #print(result)
        self.stream.write(result.encode('UTF-8'))

    def draw_image(self, gc, x, y, im):
        # use \includegraphics
        # hard, im does not include file path
        # probably need to write out png file, ugly
        pass

    def option_image_nocomposite(self):
        return True # dont like raster images

    def option_scale_image(self):
        return True # vector based, can be scaled

    def draw_text(self, gc, x, y, s, prop, angle, ismath=False):
        # should ignore prop, not useful, just use TeX commands
        # \node [rotate=angle] (x,y) {s};
        # plus gc settings
        s = r"\fontsize{{{:f}}}{{0}}\selectfont ".format(prop.get_size_in_points()) + s
        #print(s, prop.get_size(), prop.get_size_in_points())
        result = (r"    \node [anchor=south west,inner sep=0pt,outer sep=0pt,rotate={:f}] at ({:f},{:f}) {{{:s}}};" + "\n").format(angle, x, y, s)
        #result += (r"\fill [red] ({:f},{:f}) circle (1pt);" + "\n").format(x, y)
        #print(result)
        self.stream.write(result.encode('UTF-8'))

    def flipy(self):
        return False # y goes up

    def get_canvas_width_height(self):
        # has no effect
        return 1, 1

    def get_text_width_height_descent(self, s, prop, ismath):
        s = r"\fontsize{{{:f}}}{{0}}\selectfont ".format(prop.get_size_in_points()) + s
        if s in self.latex_cache:
            return self.latex_cache[s]
        self.latex.sendline(r'\setbox\my=\hbox{{{:s}}}'.format(s))
        self.latex.expect(r'\*')
        self.latex.sendline(r'\showthe\wd\my')
        self.latex.expect(r'\?')
        w = float(self.latex.before.split('\r\n')[-4][2:][:-3])
        self.latex.sendline('')
        self.latex.expect(r'\*')
        self.latex.sendline(r'\showthe\ht\my')
        self.latex.expect(r'\?')
        h = float(self.latex.before.split('\r\n')[-4][2:][:-3])
        self.latex.sendline('')
        self.latex.expect(r'\*')
        self.latex.sendline(r'\showthe\dp\my')
        self.latex.expect(r'\?')
        d = float(self.latex.before.split('\r\n')[-4][2:][:-3])
        self.latex.sendline('')
        self.latex.expect(r'\*')
        self.latex_cache[s] = (w, h + d, d)
        #print(s, w, h, d)
        return w, h + d, d # pt

    def new_gc(self):
        return GraphicsContextTikz()

    def points_to_pixels(self, points):
        return points # vector based

class GraphicsContextTikz(GraphicsContextBase):
    pass

def new_figure_manager(num, *args, **kwargs):
    FigureClass = kwargs.pop('FigureClass', Figure)
    thisFig = FigureClass(*args, **kwargs)
    canvas = FigureCanvasTikz(thisFig)
    manager = FigureManagerTikz(canvas, num)
    return manager

class FigureCanvasTikz(FigureCanvasBase):
    def __init__(self, figure):
        FigureCanvasBase.__init__(self, figure)
        self.renderer = RendererTikz()

    def get_renderer(self):
        return self.renderer

    def draw(self, stream):
        self.renderer.set_stream(stream)
        self.figure.set_dpi(72.27)
        self.figure.draw(self.renderer)

    filetypes = FigureCanvasBase.filetypes.copy()
    filetypes['tikz'] = 'tikzpicture environment'
    filetypes['tex'] = 'full TeX file'

    def print_tikz(self, stream, *args, **kwargs):
        if isinstance(stream, str):
            stream = open(stream, "wb")
        stream.write((r"  \begin{tikzpicture}[x=1pt,y=1pt]" + "\n").encode("UTF-8"))
        self.draw(stream)
        stream.write((r"  \end{tikzpicture}" + "\n").encode("UTF-8"))
        stream.flush()

    def print_tex(self, stream, *args, **kwargs):
        if isinstance(stream, str):
            stream = open(stream, "wb")
        stream.write((r"\input{{{:s}}}".format(_header) + "\n").encode("UTF-8"))
        stream.write((r"\begin{document}" + "\n" + r"  \noindent" + "\n").encode("UTF-8"))
        self.print_tikz(stream, *args, **kwargs)
        stream.write(r"\end{document}".encode("UTF-8"))
        stream.flush()

    def get_default_filetype(self):
        return 'tex'

class FigureManagerTikz(FigureManagerBase):
    pass

FigureManager = FigureManagerTikz

import matplotlib.tight_bbox
matplotlib.tight_bbox._adjust_bbox_handler_d['tex'] = matplotlib.tight_bbox.adjust_bbox_png
matplotlib.tight_bbox._adjust_bbox_handler_d['tikz'] = matplotlib.tight_bbox.adjust_bbox_png
