import pickle
import sys

import numpy as np
import pandas
import uncertainties.unumpy as unp

titles = pandas.read_csv(sys.argv[1], delim_whitespace=True, nrows=1, header=None).values[0]
t, ff, δff = np.loadtxt(sys.argv[1], skiprows=5, unpack=True)
ff = unp.uarray(ff, δff)

pickle.dump((t, ff, titles), open(sys.argv[2], 'wb'))
