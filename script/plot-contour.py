import math
import pickle
import sys

import numpy as np
from uncertainties.unumpy import nominal_values as vals, std_devs as stds

import matplotlib as mpl
import matplotlib.pyplot as plt

N, α, pairs, contours = pickle.load(open(sys.argv[1], 'rb'))
if N == 0:
    open(sys.argv[2], "w").close()
    exit()

fig = plt.figure()
if N == 2:
    fig.set_figheight(7 / 5 * math.ceil(len(pairs) / 2))
else:
    fig.set_figheight(9 / 5 * math.ceil(len(pairs) / 2))

for i, ((i1, j1), (i2, j2)) in enumerate(pairs):
    ax = fig.add_subplot(math.ceil(len(pairs) / 2), 2, i + 1)
    ax.add_patch(mpl.patches.Polygon(contours[i][1], closed=True, color='b'))
    ax.add_patch(mpl.patches.Polygon(contours[i][0], closed=True, color='g'))
    ax.plot(vals(α[i1 - 1][j1]), vals(α[i2 - 1][j2]), 'rx')
    ax.set_xlabel("$α_{{{}, {}}}$".format(i1, j1))
    ax.set_ylabel("$α_{{{}, {}}}$".format(i2, j2))
    ax.tick_params(labelsize=8)

# needs to be called twice
fig.tight_layout(h_pad=0, w_pad=0)
if N == 2:
    fig.tight_layout(h_pad=8.5, w_pad=28)
else:
    fig.tight_layout(h_pad=0, w_pad=28)
fig.savefig(sys.argv[2])
