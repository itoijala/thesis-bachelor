import pickle
import sys

import numpy as np

from uncertainties.unumpy import nominal_values as vals, std_devs as stds

from labtools.table import SymbolColumnTable, SymbolRowTable, Table, Row, Column, VerticalSpace

from common import m1, func_T

N, (α1, α2), χ2, f, *_ = pickle.load(open(sys.argv[1], "rb"))

tab1 = SymbolColumnTable()
tab1.add_si_column('i', np.arange(len(α1)), places=0)
tab1.add_si_column('α_{1, i}', α1, figures=2)
tab1.add(VerticalSpace())
tab1.add_si_column('α_{2, i}', α2, figures=2)

tab2 = SymbolRowTable()
tab2.add_si_row('χ^2', χ2, figures=4)
tab2.add_si_row('f', f, places=0)

tab = Table()
tab.add(Row())
tab.add_hrule()
tab.add(Row())
tab.add(Column([tab1, tab2], "@{}c@{}"))
tab.savetable(sys.argv[2])

T0 = func_T(m1, 0, α1)
open(sys.argv[3], 'w').write(r"{:.2f}".format(float(vals(T0))))
open(sys.argv[4], 'w').write(r"{:.2f}".format(float(stds(T0))))
