import pickle
import sys

import numpy as np
import pandas
import uncertainties as unc
from uncertainties.unumpy import nominal_values as vals, std_devs as stds

import matplotlib as mpl
import matplotlib.pyplot as plt

from common import *

N, α, *_ = pickle.load(open(sys.argv[1], "rb"))

tV, V, *_ = pickle.load(open(sys.argv[2], 'rb'))
tA1, A1, *_ = pickle.load(open(sys.argv[3], 'rb'))

x = np.linspace(0, tm, 100)
T1_α = func_T(m1, x, α[0])
T2_α = func_T(m2, x, α[1])
quot = T1_α / T2_α

tV_A1 = (tV + tA1) / 2
V_A1 = V / A1

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.plot(x, vals(quot), 'b-', zorder=1.75, label=r'$\frac{T_1}{T_2}$')
ax.fill_between(x, vals(quot) + stds(quot), vals(quot) - stds(quot), edgecolor='none', facecolor='b', alpha=0.5, zorder=1.5)
ax.fill_between(x, vals(quot) + 2 * stds(quot), vals(quot) - 2 * stds(quot), edgecolor='none', facecolor='b', alpha=0.2, zorder=1.5)
ax.errorbar(tV_A1, vals(V_A1), yerr=stds(V_A1), fmt='rx', label=r'$\frac{V}{A_1}$')

# stop line from butting into empty space
ax.add_patch(mpl.patches.Rectangle((tm, 1), 0.2, 2.5, edgecolor='none', facecolor='w', zorder=2))
# axes zorder is 2.5
ax.set_axisbelow(False)

ax.set_xlim(0, 20)
ax.set_ylim(1, 3.5)
ax.set_xlabel(r"$\silabel{t}{\giga\electronvolt\squared}$")
ax.legend(loc='upper left')
fig.savefig(sys.argv[4])
