import numpy as np

from labtools.table import SymbolRowTable

from common import *

tab = SymbolRowTable(right=True)
tab.add_si_row(r'm_{\PB}', mB * 1e3, r'\mega\electronvolt', places=0)
tab.add_si_row(r'm_{\PKstar}', mK * 1e3, r'\mega\electronvolt', places=0)
tab.add_hrule()
tab.add_si_row('t_+', tp, r'\giga\electronvolt\squared', figures=4)
tab.add_si_row('t_-', tm, r'\giga\electronvolt\squared', figures=4)
tab.add_si_row('t_0', t0, r'\giga\electronvolt\squared', figures=4)
tab.add_hrule()
tab.add_si_row('m_1', m1, r'\giga\electronvolt', places=2)
tab.add_si_row('m_2', m2, r'\giga\electronvolt', places=2)
tab.savetable('build/table/constants.tex')

open('build/result/lcsr.tex', 'w').write(r"{:.2f}".format(T_lcsr))
open('build/result/lcsr-error.tex', 'w').write(r"^{{{:.2f}}}_{{{:.2f}}}".format(δT_lcsrp, δT_lcsrm))
