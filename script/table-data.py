import pickle
import sys

import numpy as np
import uncertainties as unc
import uncertainties.unumpy as unp

from labtools.table import SymbolColumnTable, VerticalSpace

t1, T1, titles1 = pickle.load(open(sys.argv[1], 'rb'))
t2, T2, titles2 = pickle.load(open(sys.argv[2], 'rb'))

tab = SymbolColumnTable()
tab.add_si_column(titles1[0], t1, r'\giga\electronvolt\squared', places=2)
tab.add_si_column(titles1[1], T1, figures=2)
tab.add_column(VerticalSpace())
tab.add_si_column(titles2[0], t2, r'\giga\electronvolt\squared', places=2)
tab.add_si_column(titles2[1], T2, figures=2)
tab.savetable(sys.argv[3])
