import pickle
import sys

import numpy as np
import pandas
import uncertainties as unc

from common import tm

def extract_point(x, d):
    return (d - x[2]) * (x[1] - x[0]) / x[3] + x[0]

def extract_error(y, d):
    return 0.5 * d * (y[1] - y[0]) / y[3]

titles = pandas.read_csv(sys.argv[1], delim_whitespace=True, nrows=1, header=None).values[0]
x, y = pandas.read_csv(sys.argv[1], delim_whitespace=True, skiprows=3, nrows=2, header=None).values
data = np.loadtxt(sys.argv[1], skiprows=5)
t, ff = [], []
for d in data:
    t.append(extract_point(x, d[0]) * tm)
    ff.append(unc.ufloat(extract_point(y, d[1]) + extract_error(y, d[2]), extract_error(y, d[2])))
t, ff = np.array(t), np.array(ff)

pickle.dump((t, ff, titles), open(sys.argv[2], 'wb'))
