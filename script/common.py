import numpy as np
from numpy.polynomial.polynomial import polyval

from uncertainties.unumpy import nominal_values as vals, std_devs as stds

m1, m2, mB, mK = np.loadtxt("data/m")
mB *= 1e-3
mK *= 1e-3
tp = (mB + mK)**2
tm = (mB - mK)**2
t0 = tp * (1 - np.sqrt(1 - tm / tp))

t_lcsr, T_lcsr, δT_lcsrp, δT_lcsrm = np.loadtxt('data/lcsr', skiprows=3)

def func_z(s):
    return (np.sqrt(tp - s) - np.sqrt(tp - t0)) / (np.sqrt(tp - s) + np.sqrt(tp - t0))

z0 = func_z(0)

def func_T(m, t, α):
    return 1 / (1 - t / m**2) * polyval(func_z(t), α)

def func_α20(α):
    α1, α2 = α

    if len(α1) > len(α2):
        α2 = np.hstack(([0], α2))
    return polyval(z0, α1 - α2)

def func_χ2(t, T, α):
    t1, t2 = t
    T1, T2 = T
    α1, α2 = α

    α2[0] = func_α20(α)
    return np.sum(((func_T(m1, t1, α1) - vals(T1)) / stds(T1))**2) \
         + np.sum(((func_T(m2, t2, α2) - vals(T2)) / stds(T2))**2)
