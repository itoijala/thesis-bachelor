import itertools
import pickle
import sys

import numpy as np

from scipy.special import erf
from scipy.stats import chi2

import uncertainties as unc
import uncertainties.unumpy as unp
from uncertainties.unumpy import nominal_values as vals, std_devs as stds

from minuit2 import Minuit2

from common import func_α20, func_χ2

N = int(sys.argv[1])

t1, T1, *_ = pickle.load(open(sys.argv[2], 'rb'))
t2, T2, *_ = pickle.load(open(sys.argv[3], 'rb'))
t = t1, t2
T = T1, T2

α1_list = ", ".join("α1" + str(i) for i in range(0, N + 1))
α2_list = ", ".join("α2" + str(i) for i in range(1, N + 1))

χ2_adapter = eval("lambda {0}, {1}: func_χ2(t, T, (np.array(({0}, )), np.array((0, {1}))))".format(α1_list, α2_list))

α_default = {("α1" + str(i)): 0.1 for i in range(0, N + 1)}
α_default.update({("α2" + str(i)): 0.1 for i in range(1, N + 1)})

minuit = Minuit2(χ2_adapter, **α_default)
minuit.migrad()
minuit.hesse()

# not necessary, errors are symmetric and equal to errors from migrad()
# minuit.minos()

α1 = [minuit.values["α1" + str(i)] for i in range(0, N + 1)]
α2 = [minuit.values["α2" + str(i)] for i in range(1, N + 1)]
values = unc.correlated_values(α1 + α2, minuit.matrix())
α1, α2 = np.split(values, [N + 1])
α2 = np.hstack(([func_α20((α1, α2))], α2))
α = α1, α2
χ2 = minuit.fval
f = len(T1) + len(T2) - (2 * N + 1)

pickle.dump((N, α, χ2, f, t, T), open(sys.argv[4], 'wb'))

if N > 0:
    pairs = list(itertools.combinations(itertools.chain(((1, i) for i in range(N + 1)), ((2, i) for i in range(1, N + 1))), 2))
    contours = []
    for i, ((i1, j1), (i2, j2)) in enumerate(pairs):
        contours.append((
            minuit.contour("α{}{}".format(i1, j1), "α{}{}".format(i2, j2), np.sqrt(chi2.ppf(erf(1/np.sqrt(2)), 2)), 100),
            minuit.contour("α{}{}".format(i1, j1), "α{}{}".format(i2, j2), np.sqrt(chi2.ppf(erf(2/np.sqrt(2)), 2)), 100)
        ))
    pickle.dump((N, α, pairs, contours), open(sys.argv[5], 'wb'))
else:
    pickle.dump((N, None, None, None), open(sys.argv[5], 'wb'))
