import pickle
import sys

import numpy as np
from uncertainties.unumpy import nominal_values as vals, std_devs as stds

import matplotlib as mpl
import matplotlib.pyplot as plt

from labtools.plot import legend_order

from common import *

N, (α1, α2), χ2, f, (t1, t2), (T1, T2) = pickle.load(open(sys.argv[1], "rb"))

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)

ax.errorbar(t1, vals(T1), yerr=stds(T1), fmt='kx', label='$T_1$ Data', zorder=2.25)
ax.errorbar(t2, vals(T2), yerr=stds(T2), fmt='r_', label='$T_2$ Data', zorder=2.25)
ax.errorbar(t_lcsr, T_lcsr, yerr=[[δT_lcsrm], [δT_lcsrp]], fmt='mo', label='LCSR', zorder=2.25)

x = np.linspace(0, tm, 100)
T_α1 = func_T(m1, x, α1)
T_α2 = func_T(m2, x, α2)

ax.plot(x, vals(T_α1), 'g-', label="$T_1$ Fit", zorder=1.75)
ax.plot(x, vals(T_α2), 'b-', label="$T_2$ Fit", zorder=1.75)
ax.fill_between(x, vals(T_α1) + stds(T_α1), vals(T_α1) - stds(T_α1), edgecolor='none', facecolor='g', alpha=0.5, zorder=1.5)
ax.fill_between(x, vals(T_α2) + stds(T_α2), vals(T_α2) - stds(T_α2), edgecolor='none', facecolor='b', alpha=0.5, zorder=1.5)
ax.fill_between(x, vals(T_α1) + 2 * stds(T_α1), vals(T_α1) - 2 * stds(T_α1), edgecolor='none', facecolor='g', alpha=0.2, zorder=1.5)
ax.fill_between(x, vals(T_α2) + 2 * stds(T_α2), vals(T_α2) - 2 * stds(T_α2), edgecolor='none', facecolor='b', alpha=0.2, zorder=1.5)

# stop line from butting into empty space
ax.add_patch(mpl.patches.Rectangle((0, 0), -0.07, 3, edgecolor='none', facecolor='w', zorder=2))
ax.add_patch(mpl.patches.Rectangle((tm, 0), 0.2, 3, edgecolor='none', facecolor='w', zorder=2))
# axes zorder is 2.5
ax.set_axisbelow(False)

ax.set_xlim(-0.3, 20)
ax.set_ylim(0, 3.5)
ax.set_xlabel(r"$\silabel{t}{\giga\electronvolt\squared}$")
legend_order(ax, [2, 3, 0, 1, 4], loc='upper left', numpoints=1)
fig.savefig(sys.argv[2])
