import itertools
import pickle
import sys

import numpy as np
import uncertainties as unc

import matplotlib as mpl
import matplotlib.pyplot as plt

N, α, *_ = pickle.load(open(sys.argv[1], "rb"))
mat = unc.correlation_matrix(np.hstack(α))

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
im = ax.matshow(mat, vmin=-1, vmax=1)
cb = fig.colorbar(im, ax=ax)
coeffs = list(itertools.chain(((1, i) for i in range(N + 1)), ((2, i) for i in range(N + 1))))
labels = ["$α_{{{}, {}}}$".format(i, j) for i, j in coeffs]
ax.xaxis.set_ticks(range(2 * (N + 1)))
ax.yaxis.set_ticks(range(2 * (N + 1)))
ax.xaxis.set_ticklabels(labels)
ax.yaxis.set_ticklabels(labels)
for (k1, (i1, j1)), (k2, (i2, j2)) in itertools.combinations_with_replacement(enumerate(coeffs), 2):
    ax.text(k1, k2, r"\num{{{:.2f}}}".format(mat[k1][k2]), ha='center', va='center', size=8, bbox={'facecolor': 'w', 'edgecolor': 'none'})
    if k1 != k2:
        ax.text(k2, k1, r"\num{{{:.2f}}}".format(mat[k2][k1]), ha='center', va='center', size=8, bbox={'facecolor': 'w', 'edgecolor': 'none'})
fig.savefig(sys.argv[2])
