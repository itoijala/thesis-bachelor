MAKEFLAGS += --no-builtin-rules --no-builtin-variables

.SUFFIXES:

.SECONDARY:

.SECONDEXPANSION:

THESIS := front abstract introduction theory fit conclusion back
THESIS := $(addprefix thesis/,$(addsuffix .tex,$(THESIS)))

TABLES := constants \
          data-T1-T2-mescia fit-1-mescia \
          data-T1-T2-wingate fit-0-wingate fit-1-wingate fit-2-wingate \
          data-V-A1-wingate
TABLES := $(addprefix build/table/,$(addsuffix .tex,$(TABLES)))

FIGURES := quotient \
           T-1-mescia matrix-1-mescia contour-1-mescia \
           T-0-wingate T-1-wingate matrix-1-wingate contour-1-wingate T-2-wingate matrix-2-wingate contour-2-wingate
FIGURES := $(addprefix build/graphic/,$(addsuffix .pdf,$(FIGURES)))

GRAPHICS := standard-model feynman
GRAPHICS := $(addprefix graphic/,$(addsuffix .tex,$(GRAPHICS)))

TALK := front introduction theory fit conclusion back backup
TALK := $(addprefix talk/,$(addsuffix .tex,$(TALK)))

TEXENV := TEXINPUTS="$(shell pwd)/build/:$(shell pwd)/:$(shell pwd)/header-components/:" max_print_line=1048576
TEXARG := --shell-escape --halt-on-error --interaction=batchmode --output-directory=build
TEX := $(TEXENV) lualatex $(TEXARG)
PYTHONENV := $(TEXENV) PYTHONPATH=./:./labtools/
PYTHON := $(PYTHONENV) python3

MATPLOTLIB := header/matplotlib.tex header/matplotlib-packages.tex

all: build/thesis.pdf build/thesis-print.pdf build/talk.pdf build/talk-extra.pdf

build/thesis.pdf: thesis.tex header/thesis.tex header/thesis-packages.tex lit.bib libraries.bib $(THESIS) $(TABLES) $(FIGURES) $(GRAPHICS) build/result/lcsr.tex build/result/lcsr-error.tex graphic/feynman.tex | build
	@./tex.sh thesis.tex

build/thesis-print.pdf: thesis.tex header/thesis.tex header/thesis-packages.tex lit.bib libraries.bib $(THESIS) $(TABLES) $(FIGURES) $(GRAPHICS) build/result/lcsr.tex build/result/lcsr-error.tex graphic/feynman.tex | build
	@./tex.sh thesis.tex thesis-print

build/talk.pdf: talk.tex header/talk.tex header/talk-packages.tex $(TALK) $(TABLES) $(FIGURES) $(GRAPHICS) | build
	@./tex.sh talk.tex

build/talk-extra.pdf: talk.tex header/talk.tex header/talk-packages.tex $(TALK) $(TABLES) $(FIGURES) $(GRAPHICS) | build
	@./tex.sh talk.tex talk-extra

build/table/constants.tex build/result/lcsr.tex build/result/lcsr-error.tex: script/table-constants.py script/common.py data/m data/lcsr | build/table
	@echo '$$(PYTHON)' $<
	@$(PYTHON) $<

build/data/%.pickle: script/extract.py data/%.raw | build/data
	@echo '$$(PYTHON)' $< data/$*.raw $@
	@$(PYTHON) $< data/$*.raw $@

build/data/%.pickle: script/extract-dummy.py data/% | build/data
	@echo '$$(PYTHON)' $< data/$* $@
	@$(PYTHON) $< data/$* $@

build/table/data-%.tex: script/table-data.py build/data/$$(word 1,$$(subst -, ,$$*))-$$(word 3,$$(subst -, ,$$*)).pickle build/data/$$(word 2,$$(subst -, ,$$*))-$$(word 3,$$(subst -, ,$$*)).pickle | build/table
	@echo '$$(PYTHON)' $< build/data/$(word 1,$(subst -, ,$*))-$(word 3,$(subst -, ,$*)).pickle build/data/$(word 2,$(subst -, ,$*))-$(word 3,$(subst -, ,$*)).pickle $@
	@$(PYTHON) $< build/data/$(word 1,$(subst -, ,$*))-$(word 3,$(subst -, ,$*)).pickle build/data/$(word 2,$(subst -, ,$*))-$(word 3,$(subst -, ,$*)).pickle $@

build/data/fit-%.pickle build/data/contour-%.pickle: script/fit.py script/common.py data/m build/data/T1-$$(lastword $$(subst -, ,$$*)).pickle build/data/T2-$$(lastword $$(subst -, ,$$*)).pickle | build/data
	@echo '$$(PYTHON)' $< $(firstword $(subst -, ,$*)) build/data/T1-$(lastword $(subst -, ,$*)).pickle build/data/T2-$(lastword $(subst -, ,$*)).pickle build/data/fit-$*.pickle build/data/contour-$*.pickle
	@$(PYTHON) $< $(firstword $(subst -, ,$*)) build/data/T1-$(lastword $(subst -, ,$*)).pickle build/data/T2-$(lastword $(subst -, ,$*)).pickle build/data/fit-$*.pickle build/data/contour-$*.pickle

build/table/fit-%.tex build/result/T0-%.tex build/result/T0-error-%.tex: script/table-fit.py build/data/fit-%.pickle | build/table
	@echo '$$(PYTHON)' $< build/data/fit-$*.pickle build/table/fit-$*.tex build/result/T0-$*.tex build/result/T0-error-$*.tex
	@$(PYTHON) $< build/data/fit-$*.pickle build/table/fit-$*.tex build/result/T0-$*.tex build/result/T0-error-$*.tex

build/graphic/T-%.pdf: script/plot-T.py script/common.py build/data/fit-%.pickle $(MATPLOTLIB) | build/graphic
	@echo '$$(PYTHON)' $< build/data/fit-$*.pickle $@
	@$(PYTHON) $< build/data/fit-$*.pickle $@

build/graphic/matrix-%.pdf: script/plot-matrix.py build/data/fit-%.pickle $(MATPLOTLIB) | build/graphic
	@echo '$$(PYTHON)' $< build/data/fit-$*.pickle $@
	@$(PYTHON) $< build/data/fit-$*.pickle $@

build/graphic/contour-%.pdf: script/plot-contour.py build/data/contour-%.pickle $(MATPLOTLIB) | build/graphic
	@echo '$$(PYTHON)' $< build/data/contour-$*.pickle $@
	@$(PYTHON) $< build/data/contour-$*.pickle $@

build/graphic/quotient.pdf: script/quotient.py build/data/fit-1-wingate.pickle build/data/V-wingate.pickle build/data/A1-wingate.pickle $(MATPLOTLIB) | build/graphic
	@echo '$$(PYTHON)' $< build/data/fit-1-wingate.pickle build/data/V-wingate.pickle build/data/A1-wingate.pickle $@
	@$(PYTHON) $< build/data/fit-1-wingate.pickle build/data/V-wingate.pickle build/data/A1-wingate.pickle $@

build build/data build/graphic build/result build/table:
	mkdir -p build/{data,graphic,result,table}

thesis:
	@./tex.sh --fast thesis.tex

thesis-print:
	@./tex.sh --fast thesis.tex thesis-print

talk:
	@./tex.sh --fast talk.tex

talk-extra:
	@./tex.sh --fast talk.tex talk-extra

clean:
	rm -rf build

.PHONY: all clean thesis thesis-print talk
