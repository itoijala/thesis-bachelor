\section{Fit}
\subsection*{Fit}
\begin{frame}
  \frametitle{Fit}
  \begin{itemize}
    \addtolength{\itemsep}{0.5\baselineskip}
    \item Fit form factors from lattice QCD data
    \item Quenched lattice data \cite{mescia}: \citeauthor{mescia}, \citeyear{mescia}
    \item Unquenched, $2 + 1$ flavour lattice data \cite{wingate, wingateB2V}: \citeauthor{wingateB2V}, \citeyear{wingateB2V}
    \item Comparison with LCSR result \cite{1006.4945v2} for $t = 0$
  \end{itemize}
\end{frame}

\subsection{Parameterisation}
\begin{frame}
  \frametitle{Parameterisation}
  \begin{itemize}
    \addtolength{\itemsep}{0.5\baselineskip}
    \item Parameterised using simplified series expansion \cite{1004.3249v4}:
      \begin{eqn}
        \f{T_i}(t) = \frac{1}{1 - \frac{t}{m_i^2}} \sum_{j = 0}^N α_{i, j} \f{z}(t)^j
    \end{eqn}
    \item Resonance masses: $\displaystyle m_i = m_{\f{\PBs}(J^P)}$: $1^-$ for $T_1$, $1^+$ for $T_2$
    \item Map $t$ to $\intoo{-1}{1}$: $\displaystyle \f{z}(t) = \frac{\sqrt{t_+ - t} - \sqrt{t_+ - t_0}}{\sqrt{t_+ - t} + \sqrt{t_+ - t_0}}$
    \item Parameters: $\displaystyle t_± = \g(m_{\PB} ± m_{\PKstar})^2$
    \item $\displaystyle t_0 = t_+ \g(1 - \sqrt{1 - \frac{t_-}{t_+}})$ optimal
  \end{itemize}
\end{frame}

\subsection{\texorpdfstring{$χ^2$}{χ²} Fit}
\begin{frame}
  \frametitle{$χ^2$ Fit}
  \begin{itemize}
    \addtolength{\itemsep}{0.5\baselineskip}
    \item $\displaystyle \f{T_1}(0) = \f{T_2}(0) \Rightarrow α_{2, 0} = α_{1, 0} + \sum_{j = 1}^N \g(α_{1, j} - α_{2, j}) \f{z}(0)^j$
    \item Minimise $\displaystyle χ^2 = \sum_{i = 1}^2 \sum_{k = 0}^{n_i} \g(\frac{\f{T_i}(t_{i, k}) - T_{i, k}}{\err{T_{i, k}}})^{\!\! 2}$
    \item Degrees of freedom: $\displaystyle f = n_1  + n_2 - \g(2 N + 1)$
  \end{itemize}
\end{frame}

\subsection{\texorpdfstring{$N = 0$}{N = 0}}
\begin{frame}
  \frametitle{$N = 0$, Unquenched}
  \centering
  \includegraphics[width=\textwidth]{graphic/T-0-wingate.pdf}
  \begin{tikzpicture}[remember picture,overlay]
    \tikzset{shift={(current page.center)}}
    \node at (1,1) {\scalebox{0.8}{\input{table/fit-0-wingate.tex}}};
  \end{tikzpicture}
\end{frame}

\subsection{\texorpdfstring{$N = 1$}{N = 1}, Quenched}
\begin{frame}
  \frametitle{$N = 1$, Quenched}
  \centering
  \includegraphics[width=\textwidth]{graphic/T-1-mescia.pdf}
  \begin{tikzpicture}[remember picture,overlay]
    \tikzset{shift={(current page.center)}}
    \node at (0.3,1) {\scalebox{0.8}{\input{table/fit-1-mescia.tex}}};
  \end{tikzpicture}
\end{frame}
\begin{frame}
  \frametitle{$N = 1$, Quenched}
  \centering
  \includegraphics[height=0.8\textheight]{graphic/matrix-1-mescia.pdf} \\
  Correlation matrix
\end{frame}
\begin{frame}
  \frametitle{$N = 1$, Quenched}
  \centering
  \includegraphics[width=\textwidth]{graphic/contour-1-mescia.pdf} \\
  \SI{68}{\percent} and \SI{95}{\percent} confidence regions
  \begin{tikzpicture}[remember picture,overlay]
    \tikzset{shift={(current page.center)}}
    \node at (2.3,-1.3) {
      \begin{minipage}{0.4\textwidth}
        \begin{itemize}
          \item Ellipses
            \begin{itemize}
              \item Expected for $χ^2$ fit
              \item[$\Rightarrow$] Symmetrical errors
            \end{itemize}
        \end{itemize}
      \end{minipage}
    };
  \end{tikzpicture}
\end{frame}

\subsection{\texorpdfstring{$N = 1$}{N = 1}, Unquenched}
\begin{frame}
  \frametitle{$N = 1$, Unquenched}
  \centering
  \includegraphics[width=\textwidth]{graphic/T-1-wingate.pdf}
  \begin{tikzpicture}[remember picture,overlay]
    \tikzset{shift={(current page.center)}}
    \node at (1,1) {\scalebox{0.8}{\input{table/fit-1-wingate.tex}}};
  \end{tikzpicture}
\end{frame}
\begin{frame}
  \frametitle{$N = 1$, Unquenched}
  \centering
  \includegraphics[height=0.8\textheight]{graphic/matrix-1-wingate.pdf} \\
  Correlation matrix
\end{frame}

\subsection{\texorpdfstring{$N = 2$}{N = 2}}
\begin{frame}
  \frametitle{$N = 2$, Unquenched}
  \centering
  \includegraphics[width=\textwidth]{graphic/T-2-wingate.pdf}
  \begin{tikzpicture}[remember picture,overlay]
    \tikzset{shift={(current page.center)}}
    \node at (1,1) {\scalebox{0.8}{\input{table/fit-2-wingate.tex}}};
  \end{tikzpicture}
\end{frame}

\subsection{Results for \texorpdfstring{$t = 0$}{t = 0}}
\begin{frame}
  \frametitle{Results for $t = 0$}
  \centering
  \input{table/t=0.tex} \\[2em]
  Best agreement for $N = 1$ unquenched
\end{frame}

\subsection{Consistency Check Using Isgur\ndash{}Wise Relations}
\begin{frame}
  \frametitle{Consistency Check Using Isgur\ndash{}Wise Relations}
  \centering
  \includegraphics[width=\textwidth]{graphic/quotient.pdf} \\
  $N = 1$ Unquenched
  \begin{tikzpicture}[remember picture,overlay]
    \tikzset{shift={(current page.center)}}
    \node at (-0.8,1.9) {$\displaystyle \frac{T_1}{T_2} = \frac{V}{A_1} + \LandauO(\frac{1}{m_{\Pbottom}})$};
  \end{tikzpicture}
\end{frame}
