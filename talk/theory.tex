\section{Theory}
\subsection*{Theory}
\begin{frame}
  \frametitle{Theory}
  \begin{itemize}
    \addtolength{\itemsep}{0.5\baselineskip}
    \item Kinematics $\Rightarrow$ physical region: $0 \approx 4 m_{\Plepton}^2 \le t \le \g(m_{\PB} - m_{\PKstar})^2$
    \item Decay amplitude $\propto \bra{\rule{0pt}{1em} \f{\PKstar}(\ten{p}_{\PKstar}, \ten{η})}\underbrace{\rule[-0.5em]{0pt}{0em} \bar{s} Γ b}_{\mathclap{\t{\textsf{Current}}}} \, \ket{\rule{0pt}{1em} \f{\PB}(\ten{p}_{\PB})}$
    \item Non-vanishing currents:
      \begin{itemize}
        \addtolength{\itemsep}{0.5\baselineskip}
        \item Dipole form factors: $Γ = \tn{σ}{_μ_ν} \tn{q}{^ν} \g(1 + γ_5)$
        \item Semileptonic form factors: $Γ = \tn{γ}{_μ} \g(1 - γ_5)$
      \end{itemize}
    \item Lorentz decomposition
      \begin{itemize}
        \addtolength{\itemsep}{0.5\baselineskip}
        \item Sum of all linearly independent terms from $\ten{p}_{\PB}$, $\ten{p}_{\PKstar}$, $\ten{η}^*$
        \item All terms proportional to $\ten{η}^*$
        \item Parity conserved
        \item $\tn{η^*}{_μ} \tn{p_{\PKstar}}{^μ} = 0$
      \end{itemize}
  \end{itemize}
\end{frame}

\begin{extraslides}
  \begin{frame}{Einstein notation, Dirac matrices}
    Einstein notation:
    \begin{itemize}
      \item Repeated indices are summed over
      \item Greek: $\tn{a}{_μ} \tn{b}{^μ} \coloneq \sum_{μ = 0}^3 \tn{a}{_μ} \tn{b}{^μ}$
      \item Latin: $\tn{a}{_i} \tn{b}{^i} \coloneq \sum_{i = 1}^3 \tn{a}{_i} \tn{b}{^i}$
    \end{itemize}
    Metric $\tn{g}{_µ_ν}$
    \begin{itemize}
      \item Particle physics: $\tn{g}{_μ_ν} = \diag(+1, -1, -1, -1)$
      \item General relativity: $\tn{g}{_μ_ν} = \diag(-1, +1, +1, +1)$
      \item $\tn{a}{_μ} = \tn{g}{_μ_ν} \tn{a}{^ν}, \quad \tn{a}{^μ} = \tn{g}{^μ^ν} \tn{a}{_ν}$
    \end{itemize}
    Dirac matrices $\tn{γ}{^μ}$: $\g(4 \times 4)$ matrices
    \begin{itemize}
      \item $\acommut{\tn{γ}{^μ}}{\tn{γ}{^ν}} \coloneq \tn{γ}{^μ} \tn{γ}{^ν} + \tn{γ}{^ν} \tn{γ}{^μ} = 2 \tn{g}{^μ^ν}$
      \item $γ_5 = \I \tn{γ}{^0} \tn{γ}{^1} \tn{γ}{^2} \tn{γ}{^3}$
      \item $\tn{σ}{^μ^ν} = \frac{\I}{2} \g(\tn{γ}{^μ} \tn{γ}{^ν} - \tn{γ}{^ν} \tn{γ}{^μ})$
    \end{itemize}
  \end{frame}
\end{extraslides}

\subsection{Dipole Form Factors \mdash{} \texorpdfstring{$T_1$, $T_2$, $T_3$}{T₁, T₂, T₃}}
\begin{frame}
  \frametitle{Dipole Form Factors \mdash{} $T_1$, $T_2$, $T_3$}
  \begin{eqns}[l]
    \braket{\PKstar}[\bar{s} \tn{σ}{_μ_ν} \tn{q}{^ν} \g(1 + γ_5) b]{\PB}
      = 2 \I \textcolor{red}{\f{T_1}(t)} \LeviCivita{_μ_ν_ρ_σ} \tn{η^*}{^ν} \tn{p_{\PB}}{^ρ} \tn{p_{\PKstar}}{^σ} \\
      \qquad\qquad {} + \textcolor{red}{\f{T_2}(t)} \g(\g(m_{\PB}^2 - m_{\PKstar}^2) \tn{η^*}{_μ} - \tn{η^*}{_ν} \tn{p_{\PB}}{^ν} \tn{\g(p_{\PB} + p_{\PKstar})}{_μ}) \\
      \qquad\qquad {} + \textcolor{red}{\f{T_3}(t)} \tn{η^*}{_ν} \tn{p_{\PB}}{^ν} \g(\tn{q}{_μ} - \frac{t}{m_{\PB}^2 - m_{\PKstar}^2} \tn{\g(p_{\PB} + p_{\PKstar})}{_μ})
  \end{eqns}
  \hfill \cite{9910221v2}
\end{frame}

\subsection{Semileptonic Form Factors \mdash{} \texorpdfstring{$V$, $A_0$, $A_1$, $A_2$, $A_3$}{V, A₀, A₁, A₂, A₃}}
\begin{frame}
  \frametitle{Semileptonic Form Factors \mdash{} $V$, $A_0$, $A_1$, $A_2$, $A_3$}
  \begin{eqns}
    \braket{\PKstar}[\bar{s} \tn{γ}{_μ} \g(1 - γ_5) b]{\PB}
      &=& - \I \g(m_{\PB} + m_{\PKstar}) \textcolor{red}{\f{A_1}(t)} \tn{η^*}{_μ} \\
      & & {} + \frac{\I \textcolor{red}{\f{A_2}(t)}}{m_{\PB} + m_{\PKstar}} \tn{η^*}{_ν} \tn{p_{\PB}}{^ν} \tn{\g(p_{\PB} + p_{\PKstar})}{_μ} \\
      & & {} + \frac{2 \I m_{\PKstar}}{t} \g(\textcolor{red}{\f{A_3}(t)} - \textcolor{red}{\f{A_0}(t)}) \tn{η^*}{_ν} \tn{p_{\PB}}{^ν} \tn{q}{_μ} \\
      & & {} + \frac{2 \textcolor{red}{\f{V}(t)}}{m_{\PB} + m_{\PKstar}} \LeviCivita{_μ_ν_ρ_σ} \tn{η^*}{^ν} \tn{p_{\PB}}{^ρ} \tn{p_{\PKstar}}{^σ}
  \end{eqns}
  \hfill \cite{9910221v2}
\end{frame}

\subsection{Form Factor Relations}
\begin{frame}
  \frametitle{Form Factor Relations}
  \begin{itemize}
    \addtolength{\itemsep}{0.5\baselineskip}
    \item $\f{T_1}(0) = \f{T_2}(0)$
    \item Isgur\ndash{}Wise Relations \cite{1006.5013v2}
      \begin{eqn}
        \begin{IEEEeqnarraybox}[][c]{rClCl}
          \f{T_1}(t) &=& κ \f{V}(t)   &+& \LandauO(\frac{1}{m_{\Pbottom}}) \\
          \f{T_2}(t) &=& κ \f{A_1}(t) &+& \LandauO(\frac{1}{m_{\Pbottom}})
        \end{IEEEeqnarraybox}
        \Rightarrow \frac{T_1}{T_2} = \frac{V}{A_1} + \LandauO(\frac{1}{m_{\Pbottom}})
      \end{eqn}
      follow from heavy quark effective theory (expansion in $\frac{Λ_{\t{QCD}}}{m_{\Pbottom}}$)
  \end{itemize}
\end{frame}
